import { QueryClient, QueryClientProvider } from 'react-query';
import Welcome from './Components/Welcome';

const App = (): JSX.Element => {
  const queryClient = new QueryClient();

  return (
    <QueryClientProvider client={queryClient}>
      <div className="App">
        <Welcome size={10} />
      </div>
    </QueryClientProvider>
  );
};

export default App;
