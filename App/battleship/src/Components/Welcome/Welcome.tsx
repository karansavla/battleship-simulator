/* eslint-disable no-unused-vars */
import { useCallback, useState } from 'react';
import { useQueryClient } from 'react-query';
import { GeneratePlayers } from '../../Core';
import { GENERATEPLAYERS_API_KEY } from '../../Core/constants';
import Button from '../Button';
import Game from '../Game';
import {
  StyledAssumptions, StyledContainer, StyledSubUl, StyledText,
} from './Welcome.styles';

type Props = {
    size: number;
}

export const Welcome = ({ size }: Props):JSX.Element => {
  const queryClient = useQueryClient();
  const [start, setGame] = useState(false);

  const { status, data, error } = GeneratePlayers(size);

  const startGame = (): void => {
    setGame(true);
  };

  return (
    <>
      <StyledContainer>
        {!!error && 'Error Occurred, Please try again after sometime'}
        <StyledText> Simulated Game of Battleship </StyledText>
        {status === 'loading' && !start && 'Searching Players'}

        {!!data && !start && `Players ${data[0]} and ${data[1]} are ready to fight`}
        {!start
      && (
      <section>
        <Button
          config={{ level: 1 }}
          name="Let the Game Begin"
          handleClick={startGame}
        />
      </section>
      )}
        {!!start && !!data && (
        <Game
          size={10}
          first={data[0]}
          second={data[1]}
        />
        )}
      </StyledContainer>
      <StyledAssumptions>
        <h1>Rules</h1>
        <ul>
          <li>
            Game shall be played on 10x10 grid.
          </li>
          <li>
            {' '}
            This shall be a two player game and would be simulated.
            Once the game begins the page cannot be refreshed.
          </li>
          <li>
            {' '}
            Ships shall be rearranged automatically, once you click
            {' '}
            <b>Re Shuffle</b>
            {' '}
            and cannot overlap.

          </li>
          <li>
            The type and number of ships allowed for each player are same.
          </li>
          <li>
            {' '}
            The round shall be complete, when both the players have played their individual turn.
          </li>
          <li>
            Game would be
            {' '}
            <b>over</b>
            {' '}
            when all the ships of a player or both the players are
            <b> destroyed/sunk</b>
            .
          </li>
        </ul>
      </StyledAssumptions>
      <StyledAssumptions>
        <h1>Assumptions</h1>
        <ul>
          <li>
            Game cannot be stopped or the page cannot be refreshed while it is in progress.
          </li>
          <li>
            Only a single game can be simulated at a time.
          </li>
          <li>
            Ships can be reshuffled any number of times before the game begins.
          </li>
        </ul>
      </StyledAssumptions>
      <StyledAssumptions>
        <h1>Notes</h1>
        <ul>
          <li>
            Application uses React for frontend and
            .NET Core for backend.
          </li>
          <li>
            Some highlights :-
            <StyledSubUl>
              <li>1. Application respects SOLID principles.</li>
              <li>2. Strategy pattern to implement different levels of simulations.</li>
              <li>3. Iterator pattern to iterate.</li>
              <li>4. Facade pattern to simulate game play.</li>
              <li>5. Singleton pattern to save games state.</li>
              <li>6. Composite pattern to pass on bombed location between objects.</li>
            </StyledSubUl>
          </li>
        </ul>
      </StyledAssumptions>
    </>
  );
};
