import styled from 'styled-components';

export const StyledContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;

export const StyledAssumptions = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: start;

    h1{
        margin: 0 20px;
        padding-top: 1.5em;
    }

    font-size: 15px;
`;

export const StyledText = styled.h1`
    text-align: center;
`;

export const StyledSubUl = styled.ul`
    list-style: none;
`;
