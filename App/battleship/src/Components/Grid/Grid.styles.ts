import styled from 'styled-components';

export const StyledGrid = styled.div`
    margin: 10px;
`;

export const StyledRow = styled.div`
    display: flex;
    flex-direction: row;

    & .ship{
        background: #cccccc;
    }

    & .bombed {
        background: #FF0000;
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 30px;
    }

    & .ship-bombed {
        animation: shake 0.5s;
        background: #32CD32;
    }

    @keyframes shake {
        0% { transform: translate(1px, 1px) rotate(0deg); }
        10% { transform: translate(-1px, -2px) rotate(-1deg); }
        20% { transform: translate(-3px, 0px) rotate(1deg); }
        30% { transform: translate(3px, 2px) rotate(0deg); }
        40% { transform: translate(1px, -1px) rotate(1deg); }
        50% { transform: translate(-1px, 2px) rotate(-1deg); }
        60% { transform: translate(-3px, 1px) rotate(0deg); }
        70% { transform: translate(3px, 1px) rotate(-1deg); }
        80% { transform: translate(-1px, -1px) rotate(1deg); }
        90% { transform: translate(1px, 2px) rotate(0deg); }
        100% { transform: translate(1px, -2px) rotate(-1deg); }
    }
`;
