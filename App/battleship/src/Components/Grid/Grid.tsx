import { useQueryClient } from 'react-query';
import _ from 'lodash';
import { AutoArrangeShips, IsSpecialPoint } from '../../Core';
import {
  BoardRequestModel, Player,
} from '../../Core/models';
import Button from '../Button';
import Cell from '../Cell';
import { StyledGrid, StyledRow } from './Grid.styles';
import { AUTOARRANGE_API_KEY } from '../../Core/constants';

type Props = {
    size: number,
    x: number,
    y: number,
    player: string,
    ply?: Player,
    simulationStarted: boolean,
}

export const Grid = ({
  size, x, y, player, ply, simulationStarted,
}: Props): JSX.Element => {
  const { status, error, data } = AutoArrangeShips({
    x, y, s: size, p: player,
  } as BoardRequestModel);
  const queryClient = useQueryClient();
  const getBoardData = (): void => {
    queryClient.invalidateQueries([AUTOARRANGE_API_KEY, x, y, size, player]);
  };

  return (
    <StyledGrid>
      {status === 'loading' && Array.from(Array(size).keys()).map((j) => (
        <StyledRow key={`${j}`}>
          {Array.from(Array(size).keys()).map((i) => (
            <Cell isShip={false} key={`t${player}${i}${j}`} seq={`${i}${j}`} />
          ))}
        </StyledRow>
      ))}

      {!!data && Array.from(Array(size).keys()).map((j) => (
        <StyledRow key={`${j}`}>
          {Array.from(Array(size).keys()).map((i) => (
            <Cell
              isShip={IsSpecialPoint({ x: i, y: j }, _.flatMap(data.ships, 'points'))}
              isBombed={!!ply && IsSpecialPoint({ x: i, y: j }, ply.board.bombed)}
              key={`${player}${i}${j}`}
              seq={`${i}${j}`}
            />
          ))}
        </StyledRow>
      ))}

      {!!error && 'Some Error Occurred'}

      <Button
        config={{
          s: size, x, y, player,
        }}
        disabled={simulationStarted}
        name="Re Shuffle"
        handleClick={getBoardData}
      />
    </StyledGrid>
  );
};
