import styled from 'styled-components';

export const StyledGameWrapper = styled.div`
    display: flex;
    flex-direction: column;
`;

export const StyledGame = styled.div`
    display: flex;
    flex-direction: row;
    width: 1440px;
    margin: 0 auto;
    padding: 20px;
    justify-content: center;
`;

export const StyledSection = styled.div`
    padding: 30px;
`;

export const StyledHeader = styled.h1`
    font-size: 20px;
    text-align: center;
`;

export const StyledSeparator = styled.div`
      border-left: thick dotted #000000;
`;

export const StyledActionPanel = styled.div`
    clear: both;
    display: flex;
    justify-content: center;
`;

export const StyledWinner = styled.p`
    font-size: 16px;
    font-weight: bold;
`;
