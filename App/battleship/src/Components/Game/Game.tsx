import { useEffect, useState } from 'react';
import { HubConnectionBuilder, HubConnection } from '@microsoft/signalr';
import Button from '../Button';
import Grid from '../Grid';
import {
  StyledActionPanel,
  StyledGame, StyledGameWrapper, StyledHeader, StyledSection, StyledSeparator, StyledWinner,
} from './Game.styles';
import { SimulateGame } from '../../Core';

type Props = {
    size: number;
    first: string;
    second: string;
}

export const Game = ({
  size, first, second,
}: Props): JSX.Element => {
  const [simulationStarted, setSimulation] = useState(false);
  const [p1, setPlayerOne] = useState();
  const [p2, setPlayerTwo] = useState();
  const [winner, setWinner] = useState();
  const [conn, setConn] = useState<HubConnection>();

  const handleSimulation = async (o: any): Promise<void> => {
    setSimulation(true);
    await SimulateGame(o.level);
  };

  useEffect(() => {
    const connection = new HubConnectionBuilder()
      .withUrl(process.env.REACT_APP_SIGNALR_API ?? '')
      .withAutomaticReconnect()
      .build();

    setConn(connection);
  }, []);

  useEffect(() => {
    if (conn) {
      conn.start()
        // eslint-disable-next-line no-unused-vars
        .then((result) => {
          conn.on('UpdateBoard', (message) => {
            if (message.id === first) {
              setPlayerOne(message);
            } else {
              setPlayerTwo(message);
            }
          });

          conn.on('GameFinished', (message) => {
            setWinner(message);
          });
        })
        .catch((e) => console.log('Connection failed: ', e));
    }
  }, [conn, first, second]);

  const handleNewGameClick = async (): Promise<void> => {
    setSimulation(false);
    window.location.reload();
  };

  return (
    <StyledGameWrapper>
      <StyledGame>
        <StyledSection>
          <StyledHeader>
            Player
            {' '}
            {first}
          </StyledHeader>
          <Grid
            size={size}
            x={0}
            y={0}
            player={first}
            simulationStarted={simulationStarted}
            ply={p1}
          />
        </StyledSection>
        <StyledSeparator />
        <StyledSection>
          <StyledHeader>
            Player
            {' '}
            {second}
          </StyledHeader>
          <Grid
            size={size}
            x={0}
            y={0}
            player={second}
            simulationStarted={simulationStarted}
            ply={p2}
          />
        </StyledSection>
      </StyledGame>
      <StyledActionPanel>
        <Button
          config={{ level: 1 }}
          name="Random Simulation"
          disabled={simulationStarted}
          handleClick={handleSimulation}
        />
        <Button
          config={{ level: 2 }}
          name="Semi-Intelligent Simulation"
          disabled={simulationStarted}
          handleClick={handleSimulation}
        />
        {!!winner && (
        <Button
          config={0}
          name="New Game"
          handleClick={handleNewGameClick}
        />
        )}
      </StyledActionPanel>
      <StyledActionPanel>
        {!!winner && <StyledWinner>{winner}</StyledWinner>}
      </StyledActionPanel>
    </StyledGameWrapper>
  );
};
