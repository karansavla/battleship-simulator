import { StyledCell } from './Cell.styles';

type Props = {
    seq: string;
    isShip: boolean;
    isBombed?: boolean;
}

export const Cell = ({ seq, isShip, isBombed = false }: Props): JSX.Element => (
  <StyledCell className={`${isShip ? 'ship' : ''}${isBombed ? ' bombed' : ''}${isShip && isBombed ? ' ship-bombed' : ''}`} data-id={seq}>{isBombed ? 'X' : ''}</StyledCell>
);
