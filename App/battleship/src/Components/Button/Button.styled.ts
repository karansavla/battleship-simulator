import styled from 'styled-components';

const StyledButton = styled.button`
  padding: 10px;
  background: #161e54;
  color: #ffffff;
  border: 1px solid #ffffff;
  border-radius: 7px;
  margin: 5px 10px;

  &[disabled] {
      pointer: none;
      background: #cccccc;
  }
`;

export default StyledButton;
