import StyledButton from './Button.styled';

type Props = {
  name: string;
  config: any;
  disabled?: boolean;
  handleClick?: any | undefined;
};

export const Button = ({
  name, config, disabled, handleClick,
}: Props): JSX.Element => {
  const onButtonClicked = (): void => {
    handleClick(config);
  };

  return (
    <StyledButton disabled={disabled} onClick={onButtonClicked}>{name}</StyledButton>
  );
};
