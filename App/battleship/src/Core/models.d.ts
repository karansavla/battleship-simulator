export class Board {
  public x: number;

  public y: number;

  public s: number;

  constructor(x: number, y: number, s: number) {
    this.x = x;
    this.y = y;
    this.s = s;
  }
}

export interface Point {
  x: number;
  y: number;
}

export interface Ship {
  name: string;
  size: number;
  alignment: number;
  start: Point;
  damaged: number;
  points: Point[];
}

export interface Boards{
  b1: BoardResponse,
  b2: BoardResponse
}

export interface BoardResponse {
  start: Point;
  end: Point;
  bombed: Array<Point>;
  ships: Ship[];
}

export interface BoardRequestModel {
  x: number;
  y: number;
  s: number;
  p: string;
}

export interface SimulateRequestModel {
  player: string;
  start: Point;
  end: Point;
  previousPoint: Point;
  hit: boolean;
  shipsDestroyed: number;
}

export interface SimulateResponseModel{
  point: Point;
  hit: boolean;
  message: string;
}

export interface GameBoards{
  b1: BoardResponse;
  b2: BoardResponse;
}

export interface Player {
  Id: string;
  board: BoardResponse;
  shipDestroyed: number;
}

export interface Game{
  P1: Player;
  P2: Player;
}
