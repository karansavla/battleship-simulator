import { QueryStatus, useQuery } from 'react-query';
import {
  AUTOARRANGE_API_KEY, GENERATEPLAYERS_API_KEY,
  LEVEL_PARAM, PLAYER_PARAM, SIZE_PARAM, X_PARAM, Y_PARAM,
} from './constants';
import {
  BoardRequestModel, BoardResponse, Point, SimulateResponseModel,
} from './models';

const baseUrl = process.env.REACT_APP_AUTOARRANGE_API;
const generatePlayersAPI = process.env.REACT_APP_GENERATEPLAYERS_API;
const simulateGameUrl = process.env.REACT_APP_SIMULATATION_API;

export class Response<T> {
    public status: QueryStatus;

    public error: Error | null;

    public data: T | undefined;

    constructor(s: QueryStatus, e: Error | null, d: T | undefined) {
      this.status = s;
      this.error = e;
      this.data = d;
    }
}

export const generatePlayers = async (
  s: number,
): Promise<Array<string>> => {
  const api: string = generatePlayersAPI?.replace(SIZE_PARAM, `${s}`) ?? '';
  const resp = await fetch(api);
  if (resp.status !== 200) throw resp.statusText;
  return (await resp.json()) as Array<string>;
};

const initiateAutoArrange = async (
  x: number,
  y: number,
  s: number,
  p: string,
): Promise<BoardResponse> => {
  const api: string = baseUrl?.replace(X_PARAM, `${x}`)
    .replace(Y_PARAM, `${y}`)
    .replace(SIZE_PARAM, `${s}`)
    .replace(PLAYER_PARAM, p) ?? '';
  const resp = await fetch(api);
  if (resp.status !== 200) throw resp.statusText;
  return (await resp.json()) as BoardResponse;
};

export const AutoArrangeShips = (m: BoardRequestModel): Response<BoardResponse> => {
  const { status, error, data } = useQuery<BoardResponse, Error>(
    [AUTOARRANGE_API_KEY, m.x, m.y, m.s, m.p],
    async () => initiateAutoArrange(m.x, m.y, m.s, m.p), {
      initialData: {} as BoardResponse,
      refetchOnWindowFocus: false,
    },
  );
  return new Response<BoardResponse>(status, error, data);
};

export const GeneratePlayers = (s: number): Response<Array<string>> => {
  const { status, error, data } = useQuery<Array<string>, Error>(
    [GENERATEPLAYERS_API_KEY, s],
    async () => generatePlayers(s), {
      initialData: ['', ''] as [string, string],
      refetchOnWindowFocus: false,
    },
  );
  return new Response<Array<string>>(status, error, data);
};

export const IsSpecialPoint = (
  p: Point, pts: Array<Point>,
): boolean => pts.filter((v) => v.x === p.x && v.y === p.y).length > 0;

export const SimulateGame = async (level: number): Promise<SimulateResponseModel> => {
  const api = simulateGameUrl?.replace(LEVEL_PARAM, `${level}`) ?? '';

  const resp = await fetch(api);
  if (resp.status !== 200) throw resp.statusText;
  return (await resp.json()) as SimulateResponseModel;
};
