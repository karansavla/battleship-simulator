namespace Core.Models
{
    public class Game
    {
        #region Variables
        private int _size;
        #endregion

        #region Properties
        public Player First { get; private set; }
        public Player Second { get; private set; }
        #endregion

        #region Method
        public Game(int size, string first, string second)
        {
            this._size = size;
            this.First = new Player(new Point { X = 0, Y = 0 }, size, first);
            this.Second = new Player(new Point { X = size + 1, Y = size + 1 }, size, second);
        }
        #endregion
    }
}