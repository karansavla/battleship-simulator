namespace Core.Models
{
    public class Ship
    {
        #region Properties
        public string Name { get; private set; }
        public int Size { get; private set; } = 0;
        public Alignment Alignment { get; private set; }
        public Point Start { get; private set; }
        public int Damaged { get; private set; }
        public Point[] Points { get; private set; }
        #endregion

        #region Ctor
        private Ship()
        {
            this.Damaged = 0;
        }
        public Ship(string name, int size) : this()
        {
            this.Name = name;
            this.Size = size;
        }

        public Ship(string name, int size, Alignment alignment, Point start) : this(name, size)
        {
            this.Alignment = alignment;
            this.Start = start;
        }

        public Ship(string name, int size, Alignment alignment, Point[] points) : this(name, size, alignment, points[0])
        {
            this.Points = points;
        }
        #endregion

        #region Methods
        public bool IsDestroyed() => this.Size == this.Damaged;

        public int Bombed() => ++this.Damaged;
        #endregion
    }
}