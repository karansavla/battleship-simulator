namespace Core.Models
{
    public class Player
    {
        #region Ctor
        public Player(Point start, int size, string id)
        {
            this.Board = new Board(start, size);
            this.Id = id;
        }

        public Player(string id, Board board)
        {
            this.Board = board;
            this.Id = id;
        }
        #endregion

        #region Properties
        public Board Board { get; set; }
        public int ShipsDestroyed { get; private set; }
        public string Id {get; private set;}
        #endregion

        #region Methods
        public bool Shoot(Point point)
        {
            var flag = false;
            (this.ShipsDestroyed, flag) = this.Board.Bomb(point);
            return flag;
        }
        #endregion
    }
}