using System.Collections.Generic;
using System.Linq;

namespace Core.Models
{
    public class Board
    {
        #region Variables
        private int _totalDestroyed;
        #endregion

        #region Properties
        public Point Start { get; private set; }
        public Point End { get; private set; }
        public IEnumerable<Ship> Ships { get; private set; }
        public List<Point> Bombed { get; set; }
        #endregion

        #region Ctor
        private Board()
        {
            this.Bombed = new List<Point>();
        }

        public Board(Point start, Point end) : this()
        {
            this.Start = start;
            this.End = end;
        }

        public Board(Point start, int size) : this(start, new Point { X = start.X + size - 1, Y = start.Y + size - 1 })
        {
        }

        public Board(Point start, Point end, IEnumerable<Ship> ships) : this(start, end)
        {
            this.Ships = ships;
        }
        #endregion

        #region Methods
        public int ShipsDestroyed() => this._totalDestroyed;
        public (int, bool) Bomb(Point point)
        {
            var flag = false;
            foreach (var ship in this.Ships)
            {
                if (ship.IsDestroyed())
                    continue;

                if ((flag = ship.Points.Contains(point)))
                {
                    ship.Bombed();
                    if (ship.IsDestroyed())
                        _totalDestroyed++;
                    break;
                }
            }
            Bombed.Add(point);
            return (_totalDestroyed, flag);
        }
        #endregion
    }
}