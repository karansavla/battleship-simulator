using System;

namespace Core.Models
{
    public struct Point
    {
        #region Properties
        public int X { get; set; }
        public int Y { get; set; }
        #endregion
    }
}