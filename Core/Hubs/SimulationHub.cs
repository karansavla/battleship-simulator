using System.Threading.Tasks;
using Core.Interfaces;
using Microsoft.AspNetCore.SignalR;

namespace Core.Hubs{
    ///<summary>
    /// SignalR Hub to pass current Game state on to the clients.
    ///</summary>
    public class SimulationHub: Hub{

        #region Variables
        #endregion

        #region Ctor
        #endregion
    }
}