using System.Collections.Generic;
using Core.Models;

namespace Core.Interfaces
{
    /// <summary>
    ///  Auto Arrange service is responsible to place uniques ships on unique positions on boards
    /// </summary>
    /// <param name="start">Starting co-ordinate of the Board</param>
    /// <param name="end">Ending co-ordinate of the Board</param>
    /// <param name="config">Dictionary Object containing ship types and sizes</param>
    /// <returns>Board Object containing the ship positions</return>
    public interface IAutoArrangeService
    {
        Board ArrangeShips(Point start, Point end);
    }
}