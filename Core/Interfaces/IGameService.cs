using System.Collections.Generic;
using Core.Models;

namespace Core.Interfaces{
    /// <summary>
    /// Game service, this service is responsible to perform all operations on a game object
    /// </summary>
    public interface IGameService{

        /// <summary>
        /// Gets the current Game state
        /// </summary>
        /// <returns>Game State</return>
        Game GetGameState();

        /// <summary>
        ///  Passing the size of the board this method initializes the Game and generates dummy player ids
        /// </summary>
        /// <param name="size">Size of the Board</param>
        /// <returns>Array of Player Ids</return>
        IEnumerable<string> GeneratePlayers(int size);
    }
}