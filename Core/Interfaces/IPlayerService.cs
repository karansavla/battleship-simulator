using Core.Models;

namespace Core.Interfaces{
    /// <summary>
    /// Player service, this service is responsible to perform all operations on a player object
    /// </summary>
    public interface IPlayerService{
        /// <summary>
        ///  Sets the Board object for a player
        /// </summary>
        /// <param name="playerId">Starting Co-ordinate</param>
        /// <param name="alignment">Whether the ship is horizontally aligned or vertically</param>
        /// <param name="size">Size of the ship</param>
        /// <returns>Array of Co-ordinates that the ship will occupy</return>
        void SetBoardForPlayer(Player player, Board board);
    }
}