
using System.Threading.Tasks;
using Core.Models;

namespace Core.Interfaces
{
    /// <summary>
    /// Simulation Service Interface, we can use this interface and implement different levels of simulation
    /// </summary>
    public interface ISimulateService
    {
        /// <summary>
        /// Defines the level of simulation service, this can be used as a key to get correct simulation service implementation
        /// </summary>
        int Level { get; }

        /// <summary>
        /// This is the core method of simulation service. It basically runs a simulation and notifies clients via websocket the current state of the game
        /// </summary>
        /// <param name="game">Current Game Object/State</param>
        Task SimulateAsync(Game game);
    }
}