using System.Collections.Generic;
using Core.Models;

namespace Core.Interfaces
{
    /// <summary>
    /// Ship Service, this service deals with ship entity.
    /// </summary>
    public interface IShipService
    {
        /// <summary>
        /// This method gets the different kind of ships allowed
        /// </summary>
        /// <returns>Dictionary object containing the different ship types and sizes</return>
        Dictionary<string, int> GetShipTypes();

        /// <summary>
        /// We pass the start point, alignment and ship size. Based on these parameters its generates ship co-ordinates
        /// </summary>
        /// <param name="start">Starting Co-ordinate</param>
        /// <param name="alignment">Whether the ship is horizontally aligned or vertically</param>
        /// <param name="size">Size of the ship</param>
        /// <returns>Array of Co-ordinates that the ship will occupy</return>
        Point[] CalcOccupiedCoords(Point start, Alignment alignment, int size);
    }
}