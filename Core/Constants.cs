namespace Core
{
    public static class Constants
    {
        public const string Carrier = "Carrier";
        public const string BattleShip = "BattleShip";
        public const string Destroyer = "Destroyer";
        public const string Submarine = "Submarine";
        public const string PatrolBoat = "PatrolBoat";
    }
}