using System;
using System.Threading.Tasks;
using Core.Hubs;
using Core.Interfaces;
using Core.Models;
using Microsoft.AspNetCore.SignalR;

namespace Core.Impls
{
    public class SimpleSimulateService : BaseSimulationService, ISimulateService
    {
        #region Properties
        public int Level => 1;
        #endregion

        #region Ctor
        public SimpleSimulateService(IHubContext<SimulationHub> simulationHub) : base(simulationHub)
        {
        }
        #endregion

        #region Implementation
        public async Task SimulateAsync(Game game)
        {
            var counter = 0;
            var turns = 0;
            while (counter < 2)
            {
                var plyr = turns % 2 != 0 ? game.First : game.Second;
                var p = GetRandomPoint(plyr);
                plyr.Shoot(p);
                await base._simulationHub.Clients.All.SendAsync("UpdateBoard", plyr);
                await Task.Delay(600);
                if ((game.First.ShipsDestroyed == 5 || game.Second.ShipsDestroyed == 5))
                {
                    ++counter;
                }
                ++turns;
            }
            await base._simulationHub.Clients.All.SendAsync("GameFinished", base.GetResult(game.First, game.Second, turns - 1));
        }

        public Point GetRandomPoint(Player plyr)
        {
            var p = new Point
            {
                X = new Random().Next(plyr.Board.Start.X, plyr.Board.End.X + 1),
                Y = new Random().Next(plyr.Board.Start.Y, plyr.Board.End.Y + 1)
            };
            if (plyr.Board.Bombed.Contains(p))
                p = GetRandomPoint(plyr);
            return p;
        }
        #endregion
    }
}