using System.Collections.Generic;
using Core.Interfaces;
using Core.Models;

namespace Core.Impls
{
    public class ShipService : IShipService
    {
        #region Variables
        public Dictionary<string, int> _ships = new Dictionary<string, int>
        {
            {Constants.Carrier, 5},
            {Constants.BattleShip, 4},
            {Constants.Submarine, 3},
            {Constants.Destroyer, 3},
            {Constants.PatrolBoat, 2}
        };
        #endregion

        public Dictionary<string, int> GetShipTypes() => this._ships;

        public Point[] CalcOccupiedCoords(Point start, Alignment alignment, int size)
        {
            var points = new Point[size];
            points[0] = start;
            // Have not separated into diff function as we will have to
            // check condition for each step in loop rather than once as we are doing here
            if (alignment == Alignment.Horizontal)
            {
                for (var i = 1; i < size; i++)
                {
                    points[i] = new Point { X = start.X + i, Y = start.Y };
                }
            }
            else
            {
                for (var i = 1; i < size; i++)
                {
                    points[i] = new Point { X = start.X, Y = start.Y + i };
                }

            }
            return points;
        }
    }
}