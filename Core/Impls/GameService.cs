using System;
using System.Collections.Generic;
using Core.Interfaces;
using Core.Models;

namespace Core.Impls
{
    public class GameService : IGameService
    {
        #region Variables
        private Game _game;

        public Game GetGameState() => _game;

        public IEnumerable<string> GeneratePlayers(int size)
        {
            var rand = new Random();
            var first = rand.Next(1, 100);
            var second = rand.Next(1, 100);
            while (first == second)
            {
                second = rand.Next(1, 100);
            }
            SetGame(size, first.ToString(), second.ToString());
            return new string[] { _game.First.Id, _game.Second.Id };
        }
        #endregion

        #region  Private
        private void SetGame(int size, string first, string second)
        {
            _game = new Game(size, first, second);
        }
        #endregion
    }
}