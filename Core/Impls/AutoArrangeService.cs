using System;
using System.Collections.Generic;
using System.Linq;
using Core.Interfaces;
using Core.Models;

namespace Core.Impls
{
    public class AutoArrangeService : IAutoArrangeService
    {

        #region Variables
        private readonly IShipService _shipService;
        private readonly Random _rand;
        private List<Point> _occupied;
        #endregion

        #region Ctor
        public AutoArrangeService(IShipService shipService)
        {
            _shipService = shipService;
            _occupied = new List<Point>();
            _rand = new Random();
        }
        #endregion

        #region Methods
        public Board ArrangeShips(Point start, Point end)
        {
            var shipsConfiguration = _shipService.GetShipTypes();
            var ships = new List<Ship>();

            foreach (var ship in shipsConfiguration)
            {
                var (pts, dir) = GetRandCoord(this._occupied, start, end, ship.Value);
                this._occupied.AddRange(pts);
                ships.Add(new Ship(ship.Key, ship.Value, dir, pts));
            }
            return new Board(start, end, ships);
        }
        #endregion

        #region Private 
        private (Point[], Alignment) GetRandCoord(List<Point> _occupied, Point start, Point end, int size)
        {
            while (true)
            {
                var dir = _rand.Next(0, 2);
                var x = _rand.Next(start.X, dir == 0 ? end.X - size + 1 : end.X);
                var y = _rand.Next(start.Y, dir == 0 ? end.Y : end.Y - size + 1);
                var p = new Point { X = x, Y = y };
                var pts = _shipService.CalcOccupiedCoords(p, (Alignment)dir, size);
                if (_occupied.Intersect(pts).Any())
                {
                    continue;
                }
                return (pts, (Alignment)dir);
            }
        }
        #endregion
    }
}