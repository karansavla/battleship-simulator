using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Hubs;
using Core.Interfaces;
using Core.Models;
using Microsoft.AspNetCore.SignalR;

namespace Core.Impls
{
    public class MediumIntelligentSimulateService : BaseSimulationService, ISimulateService
    {
        #region Properties
        public int Level => 2;
        #endregion

        #region Ctor
        public MediumIntelligentSimulateService(IHubContext<SimulationHub> simulationHub) : base(simulationHub)
        {
        }
        #endregion

        #region Implementation
        public async Task SimulateAsync(Game game)
        {
            var counter = 0;
            var turns = 0;
            var p1HitMap = (0, new Point());
            var p2HitMap = (0, new Point());
            while (counter < 2)
            {
                var p1Turn = turns % 2 != 0;
                var plyr = p1Turn ? game.First : game.Second;
                var hitMap = p1Turn ? p1HitMap : p2HitMap;
                var count = 0;
                var p = GetRandomPoint(plyr, hitMap, turns, count);
                var flag = plyr.Shoot(p);
                if (flag)
                {
                    if (p1Turn)
                    {
                        p1HitMap = (turns, p);
                    }
                    else
                    {
                        p2HitMap = (turns, p);
                    }
                }
                await base._simulationHub.Clients.All.SendAsync("UpdateBoard", plyr);
                await Task.Delay(600);
                if ((game.First.ShipsDestroyed == 5 || game.Second.ShipsDestroyed == 5))
                {
                    counter++;
                }
                ++turns;
            }
            await base._simulationHub.Clients.All.SendAsync("GameFinished", base.GetResult(game.First, game.Second, turns - 1));
        }

        public Point GetRandomPoint(Player plyr, (int, Point) hitMap, int turn, int count = 0)
        {
            var max = hitMap.Item1 == 0 ? int.MinValue : hitMap.Item1;
            var rand = new Random();
            var p = new Point();
            if (++count < 4)
            {
                var horizontal = rand.Next(0, 2) == 0;
                var val = 0;
                do { val = rand.Next(-1, 1); } while (val == 0);
                p = new Point
                {
                    X = hitMap.Item2.X + (horizontal ? val : 0),
                    Y = hitMap.Item2.Y + (!horizontal ? val : 0)
                };
            }
            else
            {
                p = new Point
                {
                    X = rand.Next(plyr.Board.Start.X, plyr.Board.End.X + 1),
                    Y = rand.Next(plyr.Board.Start.Y, plyr.Board.End.Y + 1)
                };
            }
            if (plyr.Board.Bombed.Contains(p))
            {
                p = GetRandomPoint(plyr, hitMap, turn, count);
            }
            return p;
        }
        #endregion
    }
}