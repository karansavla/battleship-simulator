using System;
using System.Collections.Generic;
using Core.Interfaces;
using Core.Models;

namespace Core.Impls
{
    public class PlayerService : IPlayerService
    {
        #region Methods
        public void SetBoardForPlayer(Player player, Board board)
        {
            player.Board = board;
        }
        #endregion
    }
}