using Core.Hubs;
using Core.Models;
using Microsoft.AspNetCore.SignalR;

namespace Core.Impls
{
    public class BaseSimulationService
    {
        #region Variables
        protected readonly IHubContext<SimulationHub> _simulationHub;
        #endregion

        #region Ctor
        public BaseSimulationService(IHubContext<SimulationHub> simulationHub)
        {
            _simulationHub = simulationHub;
        }
        #endregion

        #region Method
        public string GetResult(Player first, Player second, int moves)
        {
            var roundsPlayed = "Total " + moves / 2 + " rounds played";
            var message = $"Player {first.Id} Won, " + roundsPlayed;
            var firstAllDestroyed = first.ShipsDestroyed == 5;
            var secondAllDestroyed = second.ShipsDestroyed == 5;
            if (firstAllDestroyed && secondAllDestroyed)
                message = "Game was a tie, " + roundsPlayed;
            else if (firstAllDestroyed && !secondAllDestroyed)
                message = $"Player {second.Id} Won, " + roundsPlayed;
            return message;
        }
        #endregion
    }
}