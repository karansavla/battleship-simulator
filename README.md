# Welcome to Battleship Game!
This is a game where two players strategically place 5 battle ships on a board of 10x10 grid. Once they have placed the ships, then the game begins. Game is divided into rounds, each player plays once in a round. Each player during their turn can bomb an area on the other players board. Bombed area must be marked. Game ends when either one player sinks/destroys all the ships of the other player or both of them destroy all the ships of each other.

## How to run the application
This is a dockerized application. Hence, to run the application, we need to navigate to the root of the application "~/battleship" and run docker-compose up

In case, if you want to run the application without docker. 

You can follow steps as below :-

	1. Navigate to "~/battleship/App/battleship" folder and run "npm run start" to run the frontend application
	2. Navigate to "~/battleship/Service" folder and run "dotnet run" to run the API application.
	

Once the applications are running, you can navigate to http://localhost:3000 to browse the game.