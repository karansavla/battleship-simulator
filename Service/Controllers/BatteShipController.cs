using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Interfaces;
using Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace Service.Controllers
{
    [ApiController]
    [Route("v1/[controller]")]
    public class BattleShipController : ControllerBase
    {
        #region Variables
        private readonly ILogger<BattleShipController> _logger;
        private readonly IAutoArrangeService _service;
        private readonly IEnumerable<ISimulateService> _simulateServices;
        private readonly IGameService _gameService;
        private readonly IPlayerService _playerService;
        #endregion

        #region Ctor
        public BattleShipController(ILogger<BattleShipController> logger, IAutoArrangeService service,
                IEnumerable<ISimulateService> services, IGameService gameService, IPlayerService playerService)
        {
            _logger = logger;
            _service = service;
            _simulateServices = services;
            _gameService = gameService;
            _playerService = playerService;
        }
        #endregion

        #region Actions

        [HttpGet]
        [Route("/")]
        public IActionResult Health()
        {
            return Ok();
        }

        [HttpGet]
        [Route("generate/{size}")]
        public IActionResult GeneratePlayers(int size)
        {
            try
            {
                return Ok(_gameService.GeneratePlayers(size));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error {nameof(GeneratePlayers)}: {ex.Message}");
                return StatusCode(500);
            }
        }

        [HttpGet]
        [Route("arrange/{x}/{y}/{size}/{player}")]
        public IActionResult AutoArrange(int x, int y, int size, string player)
        {
            try
            {
                var start = new Point { X = x, Y = y };
                var end = new Point { X = x + size - 1, Y = y + size - 1 };
                var board = _service.ArrangeShips(start, end);
                var game = _gameService.GetGameState();
                _playerService.SetBoardForPlayer(game.First.Id == player ? game.First : game.Second, board);
                return Ok(board);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error {nameof(AutoArrange)}: {ex.Message}");
                return StatusCode(500); ;
            }
        }

        [HttpGet]
        [Route("simulate/{level}")]
        public async Task<IActionResult> Simulate([FromRoute] int level)
        {
            try
            {
                var service = _simulateServices.FirstOrDefault(x => x.Level == level);
                if (service == null)
                {
                    _logger.LogError($"No such level exists {level}");
                    throw new ArgumentException("Please specify correct level");
                }
                await service.SimulateAsync(_gameService.GetGameState());
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error {nameof(Simulate)}: {ex.Message}");
                return StatusCode(500); ;
            }
        }

        [HttpGet]
        [Route("game/status")]
        public IActionResult GetGameStatus()
        {
            try
            {
                return Ok(_gameService.GetGameState());
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error {nameof(GetGameStatus)}: {ex.Message}");
                return StatusCode(500);
            }
        }
        #endregion
    }
}
