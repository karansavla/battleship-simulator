using NUnit.Framework;
using Core.Impls;
using Core.Interfaces;
using Core.Models;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace Tests.Services
{
    public class AutoArrangeServiceTests
    {
        //We could have mocked shiping service but here, we need not.
        [Test]
        [TestCase(0, 0, 9, 9)]
        public void ArrangeShips_Correctly(int x1, int y1, int x2, int y2)
        {
            //Arrange
            var service = new AutoArrangeService(new ShipService());

            //Act
            var board = service.ArrangeShips(new Point { X = x1, Y = y1 }, new Point { X = x2, Y = y2 });

            //Assert
            Assert.IsNotNull(board);
            Assert.IsTrue(board.Ships.Count() == 5);
            var list = new List<Point>();
            foreach (var s in board.Ships)
                list.AddRange(s.Points);
            Assert.IsTrue(list.Count == 17);
        }
    }
}
