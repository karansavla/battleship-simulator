using NUnit.Framework;
using Core.Impls;
using Core.Models;
using System.Linq;

namespace Tests.Services
{
    public class ShipServiceTests
    {
        [Test]
        [TestCase(0, 0, 0, 5)]
        [TestCase(0, 0, 0, 3)]
        [TestCase(0, 0, 1, 5)]
        [TestCase(0, 0, 1, 2)]
        public void Get_Ship_Coords(int x, int y, int dir, int size)
        {
            //Arrange
            var service = new ShipService();
            //Act
            var points = service.CalcOccupiedCoords(new Point { X = x, Y = y }, (Alignment)dir, size);

            //Assert
            Assert.IsNotNull(points);
            Assert.IsTrue(points.Length == size);
            Assert.IsTrue(dir == 0
                ? points.Select(x => x.Y).Sum() / size == y
                : points.Select(x => x.X).Sum() / size == x);
        }
    }
}
